import React from "react";
import { CardContainer, Number } from "./styles";

const Card = (props) => {
  const test = (e) => {
    props.clickable && props.onClick(props.cardID, props.pairID);
  };
  return (
    <CardContainer onClick={test} active={props.active}>
      {(props.active || !props.clickable) && (
        <Number className="text-center m-auto mt-5">{props.pairID}</Number>
      )}
    </CardContainer>
  );
};

export default Card;
