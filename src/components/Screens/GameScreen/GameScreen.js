import React from "react";
import { connect } from "react-redux";
import * as GameActions from "../../../store/actions/gameActions";
import { Row, Great, GameOver } from "./styles";
import Card from "./Card";
import { Modal, ProgressBar } from "react-bootstrap";
import {} from "bootstrap";

const SPLITAFTER = 4;

class GameScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPaused: this.props.isPaused,
      firstActive: -1,
      firstActiveCard: null,
      secondActiveCard: null,
      progress: 0,
      gameOver: false,
    };

    this.renderGameField = this.renderGameField.bind(this);
    this.handleCardClicked = this.handleCardClicked.bind(this);
    this.handleNewGameButton = this.handleNewGameButton.bind(this);
  }
  componentDidMount() {}

  renderGameField() {
    let rowCount = this.props.gameMode / SPLITAFTER;
    let rows = [];
    let cards = [...this.props.cards];
    for (let i = 0; i < rowCount; i++) {
      let temp = [];
      for (let j = 0; j < SPLITAFTER; j++) {
        let item = cards.shift();
        temp.push(item);
      }
      rows.push(temp);
    }
    return rows;
  }

  handleCardClicked(cardID, pairID) {
    if (this.state.firstActive === -1) {
      this.setState({ firstActiveCard: cardID, firstActive: pairID });
    } else {
      if (this.state.firstActiveCard === cardID) return;
      if (pairID === this.state.firstActive) {
        this.setState(
          {
            secondActiveCard: cardID,
          },
          () => {
            setTimeout(() => {
              this.setState({
                firstActiveCard: null,
                firstActive: -1,
                secondActiveCard: null,
              });
              this.props.solvedPair(pairID);
            }, 400);
          }
        );
      } else {
        this.setState(
          {
            secondActiveCard: cardID,
          },
          () => {
            setTimeout(() => {
              this.setState({
                firstActiveCard: null,
                firstActive: -1,
                secondActiveCard: null,
              });
              this.props.addFailed();
            }, 800);
          }
        );
      }
    }
  }

  handleNewGameButton(e) {
    e.preventDefault();
    this.props.quitGame();
  }

  //Dynamic function progress Bar
  componentDidMount() {
    let counter = 1;
    const interval = setInterval(() => {
      counter++;
      this.setState({
        progress: counter,
      });
      if (counter == 1000) {
        clearInterval(interval);
        this.setState({
          gameOver: true,
        });
      }
    }, 1000);
  }

  render() {
    let rows = this.renderGameField();
    const now = 60;
    return (
      <>
        <h1>Memory Game</h1>
        {this.state.gameOver ? (
          <GameOver>
            <h3 className="text-red">game Over try again ? </h3>
            <div className="text-center">
              <button onClick={this.handleNewGameButton} className="my-3 pl-5">
                New Game
              </button>
            </div>
          </GameOver>
        ) : (
          <div>
            {this.props.remainingPairs.length === 0 && (
              <div>
                <Great>Great, ALL SOLVED!</Great>
                <div className="text-center">
                  <button
                    onClick={this.handleNewGameButton}
                    className="my-3 pl-5"
                  >
                    New Game
                  </button>
                </div>
              </div>
            )}
            {rows &&
              rows.map((el, ix) => {
                return (
                  <Row key={"row_" + ix}>
                    {el.map((elem, idx) => {
                      let active =
                        elem.cardID === this.state.firstActiveCard ||
                        elem.cardID === this.state.secondActiveCard;
                      return (
                        <Card
                          key={"card_" + idx}
                          active={active}
                          clickable={this.props.remainingPairs.includes(
                            elem.pairID
                          )}
                          pairID={elem.pairID}
                          cardID={elem.cardID}
                          onClick={this.handleCardClicked}
                        />
                      );
                    })}
                  </Row>
                );
              })}
            <div>
              <h4>Tries: {this.props.tries}</h4>
            </div>
            <div className="w-75 text-center my-5 m-auto">
              <ProgressBar
                variant="danger"
                now={this.state.progress}
                label={`${this.state.progress}%`}
              />
            </div>
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isPaused: state.gameReducer.isPaused,
    gameMode: state.gameReducer.gameMode,
    tries: state.gameReducer.tries,
    cards: state.cardReducer.allCards,
    remainingPairs: state.cardReducer.remainingPairIDs,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setGameMode: (mode) => dispatch(GameActions.setGameMode(mode)),
    solvedPair: (pairID) => dispatch(GameActions.solvedPair(pairID)),
    addFailed: () => dispatch(GameActions.addFailedTry()),
    quitGame: () => dispatch(GameActions.quitGame()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen);
