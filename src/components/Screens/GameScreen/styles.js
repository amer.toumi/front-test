import styled from "styled-components";

export const Row = styled.div`
  margin-bottom: 10px;
  height: 100px;
  display: flex;
`;

export const CardContainer = styled.div`
  display: inline-block;
  width: 100px;
  height: 100px;
  background: orange;
  margin-right: 10px;
  transform: ${(props) => (props.active ? "scale(1.1)" : "scale(1)")};
  transition: transform 0.2s ease-in;
`;

export const Great = styled.h2`
  color: green;
`;

export const GameOver = styled.div`
  color: red;
`;

export const Number = styled.h1`
  font-size: 20px;
  font-family: bold
  color: red;
`;
